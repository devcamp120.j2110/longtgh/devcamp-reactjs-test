import 'bootstrap/dist/css/bootstrap.min.css';
import FilterComponent from './components/FilterComponent';
import OrderTable from './components/OrderTable';

import {useState, useEffect} from 'react';

function App() {  
  const fetchApi = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions);
    const responseData = await response.json();
    return responseData;
  } 

  const [filterObj, setFilterObj] = useState(null);
  const [order, setOrder] = useState([]);

  useEffect(() => {
    fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders/")
      .then((data)=>{
        if (!filterObj || (filterObj.trangThai === "None" && filterObj.loaiPizza === "None")) setOrder(data);
        else {
          let vArray = data.filter(function(el){
            if ((filterObj.trangThai !== "None") && (filterObj.loaiPizza !== "None")) // filter both pizza + combo
                    return el.trangThai === filterObj.trangThai && el.loaiPizza === filterObj.loaiPizza;
            else
                    return el.trangThai === filterObj.trangThai || el.loaiPizza === filterObj.loaiPizza; 
          })
          setOrder(vArray);
        }
      })
      .catch((err) => console.log(err))
    
  }, [filterObj, order])

  return (
    <>      
      <FilterComponent sendFilter={setFilterObj}/>
      <OrderTable data={order}/>
    </>
  );
}

export default App;
